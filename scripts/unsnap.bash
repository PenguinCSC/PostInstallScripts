#! /bin/bash
## Remove and disable Snap
# Remove installed Snaps
snap remove --purge firefox
snap remove --purge snap-store
snap remove --purge gnome-3-38-2004
snap remove --purge gtk-common-themes
snap remove --purge snapd-desktop-integration
snap remove --purge bare
snap remove --purge core20
snap remove --purge snapd

# Remove the Snap Daemon
apt -y purge snapd

# Install (non-snap) GNOME Software
apt -y install --install-suggests gnome-software