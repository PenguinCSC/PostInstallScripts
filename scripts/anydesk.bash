#! /bin/bash

## Add the AnyDesk Remote Desktop Client - https://anydesk.com/en/downloads/linux ##

# Add Repository
wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY > /etc/apt/trusted.gpg.d/anydesk.asc
echo "deb http://deb.anydesk.com/ all main" > /etc/apt/sources.list.d/anydesk-stable.list

# Install latest client
apt update
apt -y install anydesk

# Disable Wayland
sed -i "/Wayland/s/^#//g"   /etc/gdm3/custom.conf