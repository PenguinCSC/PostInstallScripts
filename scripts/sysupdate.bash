#! /bin/bash

clear

# DEB-GET Related Updates and Cleanup
echo -e "\e[93mPerforming \e[92mDEB-GET\e[93m Related Updates and Cleanup\e[0m\n"
echo -e "\e[96mUpdating Repositories\e[0m\n"
deb-get Update
echo -e "\n\e[96mUpgrading Installed Packages\e[0m\n"
deb-get upgrade

# APT Related Updates and Cleanups
echo -e "\n\e[93mPerforming \e[92mAPT\e[93m Related Updates and Cleanup\e[0m\n"
echo -e "\e[96mUpdating Repositories\e[0m\n"
apt update
echo -e "\n\e[96mUpgrading Installed Packages\e[0m\n"
apt -y upgrade
apt -y full-upgrade
echo -e "\n\e[96mCleaning Up...\e[0m\n"
apt -y autoremove
apt -y autoclean

# Flatpak Updates and Cleanup
echo -e "\n\e[93mPerforming\e[92m Flatpak\e[93m Related Updates and Cleanup\e[0m"
echo -e "\n\e[96mUpgrading Installed Flatpaks\e[0m\n"
flatpak -y update
echo -e "\n\e[96mCleaning Up...\e[0m\n"
flatpak -y uninstall --unused

# check if needs restart
echo -e "\n\e[93mChecking if we need a \e[92mReboot\e[93m... \e[0m\n"
if [ -e /var/run/reboot-required ]
then
    echo -e "\e[91;5m!! RESTART REQUIRED !!\e[0m\n"
else
    echo -e "\e[97mNo Need to Restart :-) \e[0m\n"
fi