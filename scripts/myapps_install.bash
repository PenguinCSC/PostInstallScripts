#! /bin/bash

## A script to install my own App picks, after all is done

    # Add Insync repository
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
    echo "deb http://apt.insync.io/ubuntu $(lsb_release -sc) non-free contrib" > /etc/apt/sources.list.d/insync.list
    
    # Add packages via Apt
    dpkg --set-selections < ./../configs/myapps_pkgs.lst
    apt-get -y dselect-upgrade