#! /bin/bash

###################################################################
#### This repository hosts a collection of BASH scripts        ###
#### I run after a standard (Vanilla) Ubuntu LTS installation  ###
##################################################################

## Pre-installation setup

    # Set BASH text color
    TXTGRN='\033[0;32m' # Green
    NC='\033[0m' # Reset - No Color

    # Configure scripts
    chmod +x ./scripts/*bash
    chmod 644 ./configs/*
    chown -R root:root $(pwd)/*
           
    # Unhide startup programs
    sed -i 's/NoDisplay=true/NoDisplay=false/g' /etc/xdg/autostart/*.desktop
            
    # Disable Apport Deamon
    (./scripts/disable_apport_daemon.bash)
    
    # Install scripts and configs
    mv ./scripts/sysupdate.bash /usr/local/bin/
    mv ./scripts/restart_required.bash /usr/local/bin/

    # Disable Wecome Wizard
    mkdir -p /etc/skel/.config
    touch /etc/skel/.config/gnome-initial-setup-done

## Install additional software

     # Download and install the latest Google Chrome Browser
     wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
     dpkg -i ./google-chrome-stable_current_amd64.deb

     # Install AnyDesk Client
     (./scripts/anydesk.bash)

     # Install Auto CPUFreq
     (./scripts/auto_cpufreq.bash)

     # Install prerequisites 
     apt -y install linux-headers-$(uname -r) dselect

    # Install DEB-GET
    curl -sL https://raw.githubusercontent.com/wimpysworld/deb-get/main/deb-get | sudo -E bash -s install deb-get
    
     # Pre-accept MS Fonts License
     echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections

    # Identify the distro and continues the installation accordingly
    case `lsb_release -sc` in
        bionic)
            # Add PPAs
            add-apt-repository --yes ppa:libreoffice/ppa
            add-apt-repository --yes ppa:oguzhaninan/stacer
                
            # Update System
            (./scripts/up_to_date.bash)
                               
            # Install additional software
              dselect update
              dpkg --set-selections < ./configs/bionic_pkgs.lst
              apt-get -y dselect-upgrade                                                          
                 ;;
        
        focal)
            # Add PPAs
            add-apt-repository --yes ppa:libreoffice/ppa

            # Update the system
            (./scripts/up_to_date.bash)
                      
            # Install additional software
            dselect update
            dpkg --set-selections < ./configs/focal_pkgs.lst
            apt-get -y dselect-upgrade

             # Making sure everything is up-to-date
            (./scripts/up_to_date.bash)
                   ;;
                 
        jammy)
            # Install additional software
            (./scripts/enable_flatpak.bash)
            (./scripts/unsnap.bash)
            dselect update
            dpkg --set-selections < ./configs/jammy_pkgs.lst
            apt-get -y dselect-upgrade

             # Making sure everything is up-to-date
            (./scripts/up_to_date.bash)         
esac
